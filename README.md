# Alarme de Invasão
**Bruna Kimura e Elisa Malzoni**

## Vídeo do projeto:
* https://youtu.be/RAZR-imaHZg

## Descrição do Projeto
A ideia do projeto é fazer um sistema de alarme de invasão, usando um microcontrolador, sensores e atuadores. Para tanto,usaremos um microcontrolador SAME70-XPLD, já para as entradas será usado um botão e um sensor de presença e para as saídas um buzzer e um LED.

O sensor quando for ativado, aciona buzzer e o led da placa.

O botão, um led externo, o da placa e o buzzer.

## Diagrama de blocos

![alt text](Projeto 1 - Mundo digital/img/Diagrama_blocos.jpeg)

![alt text](Projeto 1 - Mundo digital/img/esquemapinos.jpg)

## Esquematico do botão e do LED

![alt text](Projeto 1 - Mundo digital/img/esquematicos.jpeg)

## Lista de materiais

* Microcontrolador SAME70-XPLD
* Botão
* Sensor de presença
* LED
* Buzzer

**Extras:**

* LCD
