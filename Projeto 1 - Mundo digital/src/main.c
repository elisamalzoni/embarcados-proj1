/**
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUT_PIO PIOC
#define BUT_PIO_ID 12
#define BUT_PIO_PIN 17
#define BUT_PIO_PIN_MASK ( 1 << BUT_PIO_PIN)

#define SENSOR_PIO PIOA
#define SENSOR_PIO_ID 10
#define SENSOR_PIO_PIN 19
#define SENSOR_PIO_PIN_MASK ( 1<< SENSOR_PIO_PIN) 

#define BUZZER_PIO PIOC
#define BUZZER_PIO_ID ID_PIOC
#define BUZZER_PIO_PIN 31
#define BUZZER_PIO_PIN_MASK ( 1<< BUZZER_PIO_PIN)

#define LED2_PIO PIOC // led azul externo
#define LED2_PIO_ID 12
#define LED2_PIO_PIN 30
#define LED2_PIO_PIN_MASK ( 1<< LED2_PIO_PIN)


/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

volatile Bool sensor_flag = false;
volatile Bool but_flag = false;
volatile Bool jump = true;

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/
void liga_buzzer(){ //liga buzzer e led da placa
	pio_clear(LED_PIO, LED_PIO_PIN_MASK);
	//pio_clear(LED2_PIO, LED2_PIO_PIN_MASK); //led externo
	pio_clear(BUZZER_PIO, BUZZER_PIO_PIN_MASK);
	for(int i = 0; i < 500; i++) {
		pio_set(BUZZER_PIO, BUZZER_PIO_PIN_MASK);
		delay_us(500);
		pio_clear(BUZZER_PIO, BUZZER_PIO_PIN_MASK);
		delay_us(500);
	}
	pio_set(LED_PIO, LED_PIO_PIN_MASK);
	//pio_clear(LED2_PIO, LED2_PIO_PIN_MASK);
}

void liga_led(void){
	pio_clear(LED2_PIO, LED2_PIO_PIN_MASK);
}
void desliga_led(void){
	pio_set(LED2_PIO, LED2_PIO_PIN_MASK);
}
void sensor_callBack(void){
	sensor_flag = true;
}

void but_callBack(void){
	but_flag = true;
}

/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	// Initialize the board clock
	sysclk_init();
		
	// Disativa WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	// Ativa  o periferico responsavel pelo controle do LED
	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(LED2_PIO_ID);

	
	pmc_enable_periph_clk(SENSOR_PIO_ID);
	pmc_enable_periph_clk(BUZZER_PIO_ID);
		
	// Inicializa leds como sa�da
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(LED2_PIO, PIO_OUTPUT_1, LED2_PIO_PIN_MASK, PIO_DEFAULT);
	
	
	
	// Configura sensor como entrada
	pio_configure(SENSOR_PIO, PIO_INPUT, SENSOR_PIO_PIN_MASK, PIO_DEFAULT);
	
	// Configura Buzzer como saida
	pio_configure(BUZZER_PIO, PIO_OUTPUT_0, BUZZER_PIO_PIN_MASK, PIO_DEFAULT);
	
	pio_enable_interrupt(SENSOR_PIO, SENSOR_PIO_PIN_MASK);
	pio_handler_set(SENSOR_PIO,SENSOR_PIO_ID, SENSOR_PIO_PIN_MASK, PIO_IT_HIGH_LEVEL, sensor_callBack);
	NVIC_EnableIRQ(SENSOR_PIO_ID);
	NVIC_SetPriority(SENSOR_PIO_ID, 3);
	
	// Configura botao como entrada
	// Ativa o periferico responsavel pelo botao
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_handler_set(BUT_PIO,BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_RISE_EDGE, but_callBack);
	
	
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 1);
	
	
	//liga_led();
	//pio_set(LED2_PIO, LED2_PIO_PIN_MASK);
	
	// super loop
	// aplicacoes embarcadas n�o devem sair do while(1).
	
	//pio_set(LED2_PIO, LED2_PIO_PIN_MASK);

	while (1) {
		
		if (sensor_flag){
			liga_buzzer();
			sensor_flag = false;
		}
		if (but_flag){
			if(jump == false){
				//pio_clear(LED2_PIO,LED2_PIO_PIN_MASK);
				liga_led();
				liga_buzzer();
			}else{
				jump = false;				
			}
			but_flag = false;
		}
		desliga_led();
	}
	return 0;
}
